﻿using BE_Apple_Musik.DTO.Role;
using BE_Apple_Musik.DTO.Schedule;
using BE_Apple_Musik.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BE_Apple_Musik.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly ScheduleRepository _scheduleRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ScheduleController(ScheduleRepository scheduleRepository, IWebHostEnvironment webHostEnvironment)
        {
            _scheduleRepository = scheduleRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_scheduleRepository.GetAll());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var roleById = _scheduleRepository.GetById(id);

                if (roleById == null)
                {
                    return NotFound();
                }

                return Ok(roleById);

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [HttpPost]
        public IActionResult CreateRole([FromBody] CreateScheduleDTO createScheduleDTO)
        {
            try
            {
                string createRole = _scheduleRepository.Create(createScheduleDTO);

                if (createRole == "error")
                {
                    return BadRequest("Failed to Create Schedule Courses");
                }


                return Ok("create Schedule for Course success");

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Failed");
            }
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateRole(int id, [FromBody] UpdateScheduleDTO updateScheduleDTO)
        {
            try
            {
                string updateRole = _scheduleRepository.Update(id, updateScheduleDTO);

                if (updateRole == "error")
                {
                    return BadRequest("Failed to Update the Role");
                }

                if (updateRole == "not_found")
                {
                    return NotFound();
                }

                return Ok($"Update Role with id: {id} Success");

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Failed");
            }
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteCategory(int id)
        {
            try
            {
                var cat = _scheduleRepository.Delete(id);
                if (cat == false)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }
    }
}
