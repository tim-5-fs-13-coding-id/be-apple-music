﻿using BE_Apple_Musik.DTO.CourseSchedule;
using BE_Apple_Musik.DTO.Role;
using BE_Apple_Musik.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BE_Apple_Musik.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CourseScheduleController : ControllerBase
    {
        private readonly CourseScheduleRepository _courseScheduleRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CourseScheduleController(CourseScheduleRepository courseScheduleRepository, IWebHostEnvironment webHostEnvironment)
        {
            _courseScheduleRepository = courseScheduleRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_courseScheduleRepository.GetAll());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var cat = _courseScheduleRepository.GetById(id);
                if (cat == null)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] CreateCourseScheduleDTO createCourseScheduleDTO)
        {
            try
            {
                string createRole = _courseScheduleRepository.Create(createCourseScheduleDTO);

                if (createRole == "error")
                {
                    return BadRequest("Failed to Create Course Schedule");
                }


                return Ok("create Course Schedule success");

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Failed");
            }
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateRole(int id, [FromBody] UpdateCourseScheduleDTO updateCourseScheduleDTO)
        {
            try
            {
                string updateRole = _courseScheduleRepository.Update(id, updateCourseScheduleDTO);

                if (updateRole == "error")
                {
                    return BadRequest("Failed to Update the Course Schedule");
                }

                if (updateRole == "not_found")
                {
                    return NotFound();
                }

                return Ok($"Update Course Schedule with id: {id} Success");

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Failed");
            }
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var cat = _courseScheduleRepository.Delete(id);
                if (cat == false)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }
    }
}
