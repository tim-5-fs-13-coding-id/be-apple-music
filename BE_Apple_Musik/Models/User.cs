﻿namespace BE_Apple_Musik.Models
{
    public class User
    {
        public int user_id {  get; set; }
        public string first_name { get; set; } = string.Empty;
        public string last_name { get; set;} = string.Empty;

        public string email { get; set; } = string.Empty;
        public string password { get; set; } = string.Empty;
        public bool is_active { get; set; } = false;
        public DateTime? activation_date { get; set; }
        public int role_id { get; set; }

        public Role Role { get; set; }
    }
}
