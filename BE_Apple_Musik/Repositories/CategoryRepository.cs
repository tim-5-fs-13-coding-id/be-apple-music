﻿using BE_Apple_Musik.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using MySql.Data.MySqlClient;

namespace BE_Apple_Musik.Repositories
{
    public class CategoryRepository
    {
        public string _connectionString;

        public CategoryRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<Category> GetAll()
        {
            List<Category> categories = new List<Category>();

            //string connStr = "server=localhost;user=root;database=apple_musik;port=3308;password=root";
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();

                string sql = "SELECT * FROM category";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int id = rdr.GetInt32("category_id");
                    string category_name = rdr.GetString("category_name");
                    string category_desc = rdr.GetString("category_description");
                    string category_icon = rdr.GetString("category_icon");
                    string category_image = rdr.GetString("category_image");

                    categories.Add(new Category
                    {
                        category_id = id,
                        category_name = category_name,
                        category_description = category_desc,
                        category_icon = category_icon,
                        category_image = category_image,
                    });

                }
                rdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
            Console.WriteLine("Done.");

            return categories;
        }

        public List<Category> GetById(int id)
        {
            List<Category> category = new List<Category>();
            MySqlConnection conn = new MySqlConnection(_connectionString);
            
            try
            {
                conn.Open();

                string sql = "SELECT * FROM category WHERE category_id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int category_id = rdr.GetInt32("category_id");
                    string category_name = rdr.GetString("category_name");
                    string category_desc = rdr.GetString("category_description");
                    string category_icon = rdr.GetString("category_icon");
                    string category_image = rdr.GetString("category_image");

                    category.Add(new Category
                    {
                        category_id = category_id,
                        category_name = category_name,
                        category_description = category_desc,
                        category_icon = category_icon,
                        category_image = category_image,
                    });

                }
                rdr.Close();
                conn.Close();
                Console.WriteLine("Done.");

                if (category.Count == 0)
                {
                    // Set the response status code to 404 (Not Found).
                    return null;
                }

                return category;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Oops Id tidak ditemukan, Gagal untuk menerima ID");
                conn.Close();
                return null;

            }

            

            
        }

        public string Create(string name, string description, string icon, string image)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO category (category_name, category_description, category_icon, category_image) VALUES (@CatName, @CatDesc, @CatIcon, @CatImage )";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CatName", name);
                cmd.Parameters.AddWithValue("@CatDesc", description);
                cmd.Parameters.AddWithValue("@CatIcon", icon);
                cmd.Parameters.AddWithValue("@CatImage", image);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                // required
                conn.Close();
            }

            
            return errorMessage;
        }

        public string Update(int id, string name, string description, string icon, string image)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "update category SET category_name=@CatName, category_description=@CatDesc, category_icon=@CatIcon, category_image=@CatImage where category_id = @id;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CatName", name);
                cmd.Parameters.AddWithValue("@CatDesc", description);
                cmd.Parameters.AddWithValue("@CatIcon", icon);
                cmd.Parameters.AddWithValue("@CatImage", image);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    return "not_found";
                }



            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }

       
            return errorMessage;
        }

        public bool Delete (int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "DELETE FROM category where category_id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if(rowsAffected < 1)
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }

            conn.Close();

            return true;
        }
    }
}
