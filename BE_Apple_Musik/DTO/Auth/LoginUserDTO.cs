﻿using System.ComponentModel;

namespace BE_Apple_Musik.DTO.Auth
{
    public class LoginUserDTO
    {
        [DefaultValue("test@email.com")]
        public string Email { get; set; } = string.Empty;
        [DefaultValue("12345678")]
        public string Password { get; set; } = string.Empty;
    }
}
