﻿namespace BE_Apple_Musik.DTO.Course
{
    public class CreateCourseDTO
    {
        public string title { get; set; } = string.Empty;
        public string description { get; set; } = string.Empty;
        public decimal price { get; set; } 
        public IFormFile image { get; set; }
        public int category_id { get; set; }
    }
}
