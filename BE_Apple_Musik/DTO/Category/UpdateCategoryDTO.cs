﻿namespace BE_Apple_Musik.DTO.Category
{
    public class UpdateCategoryDTO
    {
        public string category_name { get; set; } = string.Empty;

        public string category_description { get; set; } = string.Empty;

        public IFormFile category_icon { get; set; }
        public IFormFile category_image { get; set; }
    }
}
