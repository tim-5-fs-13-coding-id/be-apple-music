﻿using BE_Apple_Musik.Models;
using BE_Apple_Musik.Repositories;

namespace BE_Apple_Musik.Services
{
    public class CategoryService
    {

        private readonly CategoryRepository _categoryRepository;

        public CategoryService(CategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public List<Category> GetAllCategories()
        {
            return _categoryRepository.GetAll();
        }
    }
}
